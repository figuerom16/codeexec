package handle

import (
	"net/http"
	"os/exec"
	"strings"
)

func GetCodeexec(w http.ResponseWriter, r *http.Request) {
	if err := h.ExecuteTemplate(w, "codeexec.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
	}
}

func PostCodeexec(w http.ResponseWriter, r *http.Request) {
	lang, code, runner := r.FormValue("lang"), r.FormValue("code"), ""
	switch lang {
	case "javascript", "typescript":
		runner = "deno code.js"
	case "python":
		runner = "python3 code.py"
	case "go":
		runner = "go run code.go"
	case "bash":
		runner = "bash code.sh"
	default:
		http.Error(w, "Invalid language", http.StatusBadRequest)
		return
	}
	arrRunner := strings.Split(runner, " ")
	bash := `#!/bin/bash
homes=$(ls /home)
count=0
for home in $homes; do
	if [ $home != u$count ]; then break; else ((count++)); fi
done
if ((count > 100)); then echo "TOO MANY USERS"; exit 1; fi
adduser --disabled-password --gecos "" u$count &> /dev/null
cat << EOF > /home/u$count/` + arrRunner[len(arrRunner)-1] + `
` + code + `
EOF
time timeout -s KILL 5 runuser u$count -l -c '` + runner + `'
skill -KILL -u u$count
deluser --remove-home u$count &> /dev/null
rm -r /tmp/* &> /dev/null
exit 0
`
	out, err := exec.Command("bash", "-c", bash).CombinedOutput()
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	w.Write(out)
}
