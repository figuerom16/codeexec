package handle

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

const StatusServerError = 555

var (
	h *template.Template
	m = make(map[string][]byte)
)

func Must() {
	h = template.Must(template.New("").ParseGlob("html/*.html"))
	files, err := filepath.Glob("markdown/*.md")
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		data, err := os.ReadFile(file)
		if err != nil {
			log.Fatal(err)
		}
		wrap := []byte("<div>\n")
		wrap = append(wrap, data...)
		wrap = append(wrap, []byte("</div>\n<script>me().innerHTML = marked.parse(me('-').textContent)</script>")...)
		m[filepath.Base(file)] = wrap
	}
}

func GetIndex(w http.ResponseWriter, r *http.Request) {
	md := r.URL.Query().Get("md")
	if md == "" {
		if err := h.ExecuteTemplate(w, "index.html", nil); err != nil {
			http.Error(w, err.Error(), StatusServerError)
		}
		return
	}
	w.Write(m[md])
}

func GetThree(w http.ResponseWriter, r *http.Request) {
	if err := h.ExecuteTemplate(w, "three.html", nil); err != nil {
		http.Error(w, err.Error(), StatusServerError)
	}
}

func GetDehart(w http.ResponseWriter, r *http.Request) {
	if err := h.ExecuteTemplate(w, "dehart.html", nil); err != nil {
		http.Error(w, err.Error(), StatusServerError)
	}
}

func GetAPIFlattener(w http.ResponseWriter, r *http.Request) {
	if err := h.ExecuteTemplate(w, "apiflattener.html", nil); err != nil {
		http.Error(w, err.Error(), StatusServerError)
	}
}

func GetMarkdown(w http.ResponseWriter, r *http.Request) {
	if err := h.ExecuteTemplate(w, "markdown.html", nil); err != nil {
		http.Error(w, err.Error(), StatusServerError)
	}
}
