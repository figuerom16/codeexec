package handle

import "net/http"

func GetDuck(w http.ResponseWriter, r *http.Request) {
	if err := h.ExecuteTemplate(w, "duckdb.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
	}
}
