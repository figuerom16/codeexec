project=mattascale

# Update and install dependencies
apt-get -y update && apt-get -y dist-upgrade
apt-get -y install curl git unzip wireguard python3-pip iptables iptables-persistent
curl -fsSL https://deno.land/install.sh | DENO_INSTALL=/usr/local sh
pip3 install --break-system-packages numpy pandas polars scipy pycrypto whoosh passlib sympy

# Test for sudo
if [ "$EUID" -ne 0 ]; then
	echo 'Error: This script must be run as sudo.' >&2
	exit 1
fi

# Setup Mattascale
cd /opt
git clone https://gitlab.com/figuerom16/$project.git
cd $project
go=$(grep ^go go.mod | tr -d ' ')
wget https://go.dev/dl/$go.linux-amd64.tar.gz
rm -rf /usr/local/go && tar -C /usr/local -xzf $go.linux-amd64.tar.gz
rm $go.linux-amd64.tar.gz
echo 'source /usr/local/etc/bash_completion.d/deno.bash' | tee -a ~/.bashrc /etc/profile
echo 'export GOCACHE=/opt/go-build' | tee -a ~/.bashrc /etc/profile
echo 'export PATH=$PATH:/usr/local/go/bin' | tee -a ~/.bashrc /etc/profile
cat << EOT > /etc/systemd/system/$project.service
[Unit]
Description=$project
After=network.target

[Service]
WorkingDirectory=/opt/$project
ExecStart=/opt/$project/$project
Restart=always

[Install]
WantedBy=multi-user.target
EOT
systemctl enable $project.service
systemctl enable wg-quick@wg0.service

# Security for Code Executor
echo '* soft nproc 32' >> /etc/security/limits.conf
echo '* hard nproc 32' >> /etc/security/limits.conf
echo '* soft nofile 128' >> /etc/security/limits.conf
echo '* hard nofile 128' >> /etc/security/limits.conf
iptables -A OUTPUT -m owner --uid-owner root -j ACCEPT
iptables -A OUTPUT -m owner --uid-owner _apt -j ACCEPT
iptables -A OUTPUT -p udp --dport 51820 -j ACCEPT
iptables -A OUTPUT -o lo -p icmp -j ACCEPT
iptables -A OUTPUT -j REJECT
iptables-save > /etc/iptables/rules.v4

#Environment Settings
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
echo "cd /opt/$project" >> ~/.bashrc

reboot

# MANUALLY Complete install:
go build std && chmod 755 /opt/go-build
chmod 777 /opt/go-build/trim.txt
go build
# Add back in Wireguard config