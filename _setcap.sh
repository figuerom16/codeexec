#!/bin/sh

# USAGE:
# 	go run -exec ./setcap.sh main.go <args...>
# 	Used for running programs as non root on ports < 1024

sudo setcap cap_net_bind_service=+ep "$1"
"$@"