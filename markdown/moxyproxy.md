## ***MoxyProxy*** 🔆

https://gitlab.com/figuerom16/moxyproxy

---

![network](https://gitlab.com/figuerom16/moxyproxy/-/raw/main/screenshots/network.png)

Viewing this website means that you're passing through MoxyProxy. This project is a personal favorite as the goal was to make an easy to use proxy that has a few special features.

✅ **Features:**
- Built using Atreugo (fasthttp). Should be faster than Caddy/Traefik (need to benchmark).
- Web Interface built into a single app.
- ACME Autocert using TLS-ALPN-01. No more certificate management.
- Ratelimit and automatically ban problem users.
- Serve assets on the proxy.
- Email Token and OAuth2 Authentication. Also allow for forwarding user information to upstream servers in Msgpack format.
- Easy wireguard management and TCP/UDP forwarding.

ℹ️ More information in the git link above.