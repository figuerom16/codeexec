## ***ThreeJS*** 🌐

⬆️ Project in top navbar above.

---

![threejs](https://mattascale.com/moxyproxy/public/threejs.png)

This one was just for fun. It was more supprising how easy ThreeJS makes it to do simple 3D animation.
The above was ~70 lines of Javascript.

[AnotherOne](/dehart) Similar simple animation.

ℹ️ **Code Link:**
- ThreeJS Code: https://gitlab.com/figuerom16/mattascale/-/blob/main/html/three.html?ref_type=heads