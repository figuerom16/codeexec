## ***TLDR:*** Proving Grounds for Ideas/Projects 🌱

https://gitlab.com/figuerom16

https://gitlab.com/users/figuerom16/projects

---

💠 This site is for testing and demonstrating projects. It is hosted behind a Reverse Proxy, which is another project called [MoxyProxy](https://gitlab.com/figuerom16/moxyproxy). The goal of these projects is to push web development without Node using the fundamentals of Webdev HTML|CSS|JS with Go as the backend then selfhosting using Proxmox.

**Click the above tabs for a short summary of each project.**

#### ♦️ Projects Short List:
- **MoxyProxy**: FastHTTP ReverseProxy with built in Auth, LoadBalancer, RateLimiter, and FileServer.
- **Code Executor**: Execute abitrary code on the server. Server has built in protections.
- **ThreeJS**: Simple fun animation using [ThreeJS](https://threejs.org)
- **API Flattener**: Queries an API for JSON then represents it in a table. Demonstration of client interactivity without Node.
- **Markdown Editor**: Simple live markdown editor.
- **Fyne GUI**: WIP Fyne GUI Apps.
- **DuckDB Scrub**: Under Construction. 🚧

##### 🔶 *Core Web Stack:*
- [Go](https://go.dev): Standard Library with html/template
- [Routegroup](https://github.com/go-pkgz/routegroup): Std router helper
- [SQLite3](https://github.com/ncruces/go-sqlite3): Easy embedded SQL Database
- [Fixi](https://github.com/bigskysoftware/fixi): Simple HTML fragment swapping
- [Surreal](https://github.com/gnat/surreal): Javascript helper similar to jQuery
- [BulmaCSS](https://github.com/jgthms/bulma): No Node TailwindCSS alternative
- [Lucide](https://github.com/lucide-icons/lucide): SVG icon rendering pack

##### 🔸 *Supporting Libraries:*
- [Playground-Form|Mold|Validator](https://github.com/go-playground): Form data scrubbing
- [Argon2ID](https://github.com/alexedwards/argon2id): Argon2ID hasher
- [Compress](https://github.com/klauspost/compress): ZSTD compression
- [Msgpack](https://github.com/vmihailenco/msgpack): Msgpack formatting
- [Code-Input](https://github.com/WebCoder49/code-input): Browser code editor
- [Highlightjs](https://highlightjs.org): Browser code highlighting
- [Marked](https://github.com/markedjs/marked): Browser Markdown renderer

##### 🔧 *Server Setup:*
- [Cloudflare](https://www.cloudflare.com/): Domain and DNS
- [ServerOptima](https://www.serveroptima.com): VPS for MoxyProxy
- [Proxmox](https://www.proxmox.com/en): Local Virtualization
- [IPFire](https://www.ipfire.org): Network Firewall
- [Debian](https://www.debian.org): LXC Templates
