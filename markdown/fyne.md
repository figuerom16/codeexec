## ***API Flattener*** 💻

https://gitlab.com/figuerom16/moxyspice

https://gitlab.com/figuerom16/shellypal

---

Fyne GUI apps written in Go.

![moxyspice](https://gitlab.com/figuerom16/moxyspice/-/raw/main/screenshots/login.png)
![moxyspice](https://gitlab.com/figuerom16/moxyspice/-/raw/main/screenshots/list.png)

MoxySpice is a helper for connecting through the spice protocol to Proxmox.

![shellypal](https://gitlab.com/figuerom16/shellypal/-/raw/main/screenshots/ssh.png)

WIP. Shellypal is a convenience terminal to quickly SSH into servers with simple one click uploads and downloads.
It's unfortunately not ready for regular use yet as [Fyne Terminal](https://github.com/fyne-io/terminal) has quite a few bugs.