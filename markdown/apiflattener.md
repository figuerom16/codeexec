## ***API Flattener*** 📥

⬆️ Project in top navbar above.

---

API Flattener is a demonstration of doing a dynamic web page without React/Svelte. Instead of using template tags in the HTML file it was easier to use Object.assign(document.createElement(),{options}) to quickly create elements for rendering. The other notable for this project is that a custom watch function is being used. What the below is doing for this project is that the values variable is being watched and when it is changed the renderSheet function is called automatically. There is a debounce to prevent multiple rerenders. It's a nice easy way to add reactivity while avoiding mutation observers since Fixi is handling those.
```js
function watch(input, handler) {
	let timeout
	if (input !== Object(input)) input = {watch: input}
	return new Proxy(input, {
		set(target, property, value, receiver) {
			if (target[property] === value) return true
			const result = Reflect.set(target, property, value, receiver)
			clearTimeout(timeout)
			timeout = setTimeout(handler, 0, target)
			return result
		}
	})
}
```

ℹ️ **Code Links:**
- HTML: https://gitlab.com/figuerom16/mattascale/-/blob/main/html/apiflattener.html?ref_type=heads
- JS: https://gitlab.com/figuerom16/mattascale/-/blob/main/static/apiflattener.js?ref_type=heads

![markdown](https://mattascale.com/moxyproxy/public/apiflattener.png)