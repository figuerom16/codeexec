## ***Code Executor*** ⚡

⬆️ Project in top navbar above.

![network](https://mattascale.com/moxyproxy/public/codeexec.png)

The Code Executor will take arbitrary code and attempts to execute it directly on the Linux server returning the output.
Right now it only has runtimes for: Deno (JS/TS), Python, Go, and Bash.
This sounds dangerous, but some safeguards have been put into place.
Below is the process for how it works.

❗ **Process:**
1. [Code-Input](https://github.com/WebCoder49/code-input) is the editor that does syntax highlighting with [Highlightjs](https://highlightjs.org).
2. As the user types [Highlightjs](https://highlightjs.org) guesses at the language.
3. When the code is Run the language and code to the server in a POST request.
4. The runner is chosen by the language then the code is wrapped in a bash script.
5. The bash script creates a new user temporary user `u#` then saves the code in the user directory.
6. The user then executes the code with the runner with a KILL timer of 5 seconds to complete.
7. The user is then cleaned up.
8. Output from the bash script, runner, and code results are returned as response bytes.
9. The response is rendered on the client.

ℹ️ **Code Links:**
- Code Handlers: https://gitlab.com/figuerom16/mattascale/-/blob/main/handle/codeexec.go?ref_type=heads
- Counter Measures: https://gitlab.com/figuerom16/mattascale/-/blob/main/_install.sh?ref_type=heads#L42
The counter measures in the install will stop Fork/File Bombs and created user from reaching the internet.