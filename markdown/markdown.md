## ***Markdown Editor*** 📙

⬆️ Project in top navbar above.

---

The editor isn't the project in itself. It's using nothing but a javascript library [Marked](https://github.com/markedjs/marked) to render all markdown. The editor is for convenience for creating the notes you are looking at right now. The markdown for the notes are then saved directly in `.md` files on the server.

The more creative part is how you are seeing the mardown files rendered in place. In Go using glob we gather all the markdown files then while inserting the byte data into a `map[string][]byte` we wrap the data in a div and a script `me().innerHTML = marked.parse(me('-').textContent)` using [Surreal](https://github.com/gnat/surreal) shorthand.

What this means is when a tab is clicked [Fixi](https://github.com/bigskysoftware/fixi) does an `fx-action='GET'` and inserts the markdown with added JS into the page, but then the script tag is executed causing [Marked](https://github.com/markedjs/marked) to render and replace itself with HTML.

It makes for easy Markdown Pages.

![markdown](https://mattascale.com/moxyproxy/public/markdown.png)