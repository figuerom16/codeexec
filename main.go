package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-pkgz/routegroup"
	"gitlab.com/figuerom16/mattascale/handle"
)

type wrappedWriter struct {
	http.ResponseWriter
	statusCode int
}

func (w *wrappedWriter) WriteHeader(status int) {
	w.ResponseWriter.WriteHeader(status)
	w.statusCode = status
}

// Log logs the request method, status code, and path.
func logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		wrapped := &wrappedWriter{w, http.StatusOK}
		next.ServeHTTP(wrapped, r)
		log.Println(r.RemoteAddr, r.Method, wrapped.statusCode, r.URL.Path, time.Since(start))
	})
}

func main() {
	// Setup
	handle.Must()

	// Router
	router := routegroup.New(http.NewServeMux())
	router.Route(func(base *routegroup.Bundle) {
		base.Use(logger)
		base.Handle("/", http.FileServer(http.Dir("./static")))
		base.HandleFunc("GET /{$}", handle.GetIndex)
		base.HandleFunc("GET /three", handle.GetThree)
		base.HandleFunc("GET /codeexec", handle.GetCodeexec)
		base.HandleFunc("POST /codeexec", handle.PostCodeexec)
		base.HandleFunc("GET /apiflattener", handle.GetAPIFlattener)
		base.HandleFunc("GET /markdown", handle.GetMarkdown)
		base.HandleFunc("GET /duckdb", handle.GetDuck)
		base.HandleFunc("GET /dehart", handle.GetDehart)
	})

	// Server Run
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("ADDRESS: http://%s", conn.LocalAddr().(*net.UDPAddr).IP.String())
	server := http.Server{Addr: ":80", Handler: router}
	go func() {
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			log.Printf("ListenAndServe error: %v", err)
			syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
		}
	}()
	// Graceful shutdown: use a signal context and a shutdown timeout.
	c, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()
	<-c.Done()
	start := time.Now()
	log.Println("Gracefully shutting down...")
	c, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := server.Shutdown(c); err != nil {
		log.Printf("Shutdown error: %v", err)
	}
	log.Printf("Gracefully closed in %s", time.Since(start))
}
