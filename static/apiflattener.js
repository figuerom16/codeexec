let apiflattener = (function() {

let jsonTree
let origValues
let mapValues = new Map()
let values = watch([], renderSheet)

async function getApi(event) {
	event.preventDefault()
	const api = event.target.elements["API"].value
	if (!api) return
	jsonTree = []
	mapValues.clear()
	const resp = await fetch(api)
	if (!resp.ok) {
		const article = document.createElement('article')
		article.className = 'message is-danger'
		const pre = document.createElement('pre')
		pre.className = 'message-body is-size-5'
		article.appendChild(pre)
		pre.textContent = resp.statusText
		const tree = me('#tree')
		tree.innerHTML = ''
		tree.appendChild(article)
		return
	}
	const json = await resp.json()
	if (!json) return
	origValues = parseJson(json)
	values.length = 0
	values.push(...origValues)
	jsonTree = []
	if (Array.isArray(json)) {
		if (json.length > 1) return
		json = json[0]
	}
	treeify('', json)
	jsonTree = jsonTree.reverse()
	let paths = jsonTree.flat()
	for (const i in paths) {
		let arr = paths[i].split('.')
		let temp = json
		for (const j in arr) {
			if (Array.isArray(temp)) temp = temp[0]
			temp = temp[arr[j]]
		}
		mapValues.set(paths[i], parseJson(temp))
	}
	//Render Tree
	const tree = me('#tree')
	tree.innerHTML = ''
	const initialRow = Object.assign(document.createElement('div'),{className:'columns'})
	const buttonRoot = createButton('JSON ROOT', 'is-primary', _=>{values.splice(0, values.length, ...origValues)})
	initialRow.appendChild(createColumn(buttonRoot))
	const buttonFlatten = createButton('JSON FLATTEN', 'is-link', flattenJson)
	initialRow.appendChild(createColumn(buttonFlatten))
	tree.appendChild(initialRow)
	jsonTree.forEach(level => {
		const row = Object.assign(document.createElement('div'),{className:'columns'})
		level.forEach(item => {
			const button = createButton(item, 'is-info is-outlined', _=>{values.splice(0, values.length, ...mapValues.get(item))})
			row.appendChild(createColumn(button))
		})
		tree.appendChild(row)
	})
}

function treeify(chain, json) {
	let level = []
	let currentChain = chain
	let key
	for (key in json) {
		let subJson = json[key]
		if (Array.isArray(subJson)) subJson = subJson[0]
		if (subJson && typeof(subJson) !== 'string' && typeof(subJson) !== 'number') {
			if (currentChain) chain = currentChain + '.' + key
			else chain = key
			level.push(chain)
			if (Array.isArray(json[key]) && json[key].length > 1) {}
			else treeify(chain, subJson)
		}
	}
	if (level.length > 0) jsonTree.push(level)
}

function parseJson(json) {
	let temp = []
	if (Array.isArray(json) && json.length > 1) {
		temp[0] = Object.keys(json[0])
		for(let i = 0; i < json.length; i++) temp[i+1] = Object.values(json[i])
		let moreJson = []
		temp = temp[0].map((_, colid)=>temp.map(row => row[colid]))
		for (const i in temp) {
			if (temp[i][1] && typeof(temp[i][1]) !== 'string' && typeof(temp[i][1]) !== 'number') {
				const keys = Object.keys(temp[i][1])
				for (const k in keys) moreJson[k] = [temp[i][0] + '_' + keys[k]]
				for (const k in keys) {
					for (let j = 1; j < temp[i].length; j++) moreJson[k][j] = temp[i][j][keys[k]]
				}
				temp.splice(i, 1)
			}
		}
		return [...temp, ...moreJson]
	}
	else if (Array.isArray(json)) json = json[0]
	return Object.entries(json)
}

function flattenJson() {
	values.splice(0, values.length)
	let maxLength = 0
	let allColumns = []
	for (let col in origValues) {
		if (typeof(origValues[col][1]) !== 'object') {
			maxLength = Math.max(maxLength, origValues[col].length)
			allColumns.push([...origValues[col]])
		}
	}
	for (const table of mapValues.values()) {
		for (let col in table) {
			if (typeof(table[col][1]) !== 'object') {
				maxLength = Math.max(maxLength, table[col].length)
				allColumns.push([...table[col]])
			}
		}
	}
	const extendedColumns = allColumns.map(col => {
		let newCol = [...col]
		if (newCol.length === 2) for (let i = 0; i < maxLength - 1; i++) newCol.push(newCol[1])
		else newCol.push(newCol[1])
		return newCol
	})
	extendedColumns.forEach(col => values.push(col));
}

function createButton(text, style, handler) {
	const button = document.createElement('button')
	button.className = 'button is-fullwidth ' + style
	button.textContent = text
	button.addEventListener('click', handler)
	return button
}

function createColumn(child) {
	const column = document.createElement('div')
	column.className = 'column'
	column.appendChild(child)
	return column
}

function renderSheet() {
	const sheet = me('#sheet')
	sheet.innerHTML = ''
	const field = Object.assign(document.createElement('div'),{className:'field is-grouped'})
	field.appendChild(createButton('Download CSV', 'is-success is-outlined', download))
	field.appendChild(Object.assign(document.createElement('input'),{id:'separator',className:'input',type:'text',placeholder:'SEPARATOR DEFAULT |'}))
	sheet.appendChild(field)
	const columns = Object.assign(document.createElement('div'),{className:'columns is-gapless'})
	sheet.appendChild(columns)
	values.forEach(value => {
		const colContent = document.createElement('div')
		value.forEach(v => {
			colContent.appendChild(Object.assign(document.createElement('div'),{className:'cell',textContent:v}))
		})
		columns.appendChild(createColumn(colContent))
	})
}

function download() {
	let outputCsv = ''
	let sep = me('#separator')
	if (sep == '') sep = '|'
	for (let i = 0; i < values[0].length; i++) {
		for (let j = 0; j < values.length; j++) {
			outputCsv += values[j][i] + sep
			if (j == values.length - 1) outputCsv = outputCsv.slice(0, -1) + '\n'
		}
	}
	const link = document.createElement("a")
	link.download = "output.csv"
	const text = new Blob([outputCsv], { type: "text/plain" })
	link.href = window.URL.createObjectURL(text)
	link.click()
}

return {
	getApi,
	download,
}
})()