let theme = localStorage.getItem('theme') || 'dark'
me("html").attr('data-theme', theme)

window.onscroll=()=>{
	if (document.documentElement.scrollTop > 100) scroller.style.display = "block"
	else scroller.style.display = "none"
}

function copyToClipboard(text) {
	if (navigator.clipboard && navigator.clipboard.writeText) {
		navigator.clipboard.writeText(text)
		return
	}
	const textarea = document.createElement('textarea')
	textarea.value = text
	document.body.appendChild(textarea)
	textarea.select()
	document.execCommand('copy')
	document.body.removeChild(textarea)
}

function exportTable(table, sep) {
	const rows = [...table.rows]
	const data = rows.filter(row => row.style.display !== 'none').map((row, index)=>[...row.cells].map(cell=>index=== 0?cell.innerText.slice(0, -2):cell.textContent))
	const blob = new Blob([data.map(row => row.join(sep)).join('\n')], {type: 'text/csv'})
	const a = document.createElement('a')
	a.href = URL.createObjectURL(blob)
	a.download = 'export.csv'
	a.click()
	URL.revokeObjectURL(a.href)
}

function showType(show, head) {
	if (show) for (let i = 0; i < head.cells.length; i++) head.cells[i].innerHTML = head.cells[i].innerHTML.replace(/<span style="display: none;">\[(.*?)\]<\/span>/g, '[$1]')
	else for (let i = 0; i < head.cells.length; i++) head.cells[i].innerHTML = head.cells[i].innerHTML.replace(/\[(.*?)\]/g, '<span style="display: none;">[$1]</span>')
}

function searchTable(table, term) {
	const rows = [...table.rows].slice(1)
	rows.forEach((row)=>{ 
		const found = [...row.cells].some(cell=>{
			if (cell.getElementsByTagName('script').length > 0) return false
			return cell.textContent.includes(term)
		})
		row.style.display = found ? '' : 'none'
	})
}

function sortTable(head) {
	const arrow = head.textContent.substr(-1)
	const heads = head.parentElement
	const column = [...heads.cells].indexOf(head)
	const body = head.parentElement.parentElement
	const rowsArray = [...body.rows].slice(1)
	for (let e of heads.cells) {if (['►','▲','▼'].includes(e.textContent.substr(-1))) e.textContent=e.textContent.slice(0, -1) + '►'}
	if (arrow === '▼') {
		head.textContent = head.textContent.slice(0, -1) + '▲'
		rowsArray.sort((a, b)=>b.cells[column].textContent.localeCompare(a.cells[column].textContent, undefined, { numeric: true }))
	}
	else {
		head.textContent = head.textContent.slice(0, -1) + '▼'
		rowsArray.sort((a, b)=>a.cells[column].textContent.localeCompare(b.cells[column].textContent, undefined, { numeric: true }))
	}
	body.replaceChildren(heads, ...rowsArray)
}

function generateKey() {
	const length = 32
	const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'
	let key = ''
	for (let i = 0; i < length; i++) key += chars[Math.floor(Math.random() * chars.length)]
	return key
}

function watch(input, handler) {
	let timeout
	if (input !== Object(input)) input = {watch: input}
	return new Proxy(input, {
		set(target, property, value, receiver) {
			if (target[property] === value) return true
			const result = Reflect.set(target, property, value, receiver)
			clearTimeout(timeout)
			timeout = setTimeout(handler, 0, target)
			return result
		}
	})
}